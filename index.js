process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const fetch = require('node-fetch')
const fs = require('fs')
fetch('https://jservice.kenzie.academy/api/categories?count=100')
    .then(responseObj => responseObj.json())
    .then(responseObj => {
        let cat = JSON.stringify(responseObj)
        console.log(cat[0].title)
        fs.writeFile('categories.json', cat, (err) => {
            if (err) throw err;
            console.log('Categories saved!');
        });
    });